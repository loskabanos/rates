# SQL Manager 2010 for MySQL 4.5.0.9
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : rates


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `rates`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `rates`;

#
# Structure for the `tcity` table : 
#

CREATE TABLE `tcity` (
  `nIdCity` int(11) NOT NULL AUTO_INCREMENT,
  `sName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nIdCity`),
  KEY `I_tCity_sName` (`sName`)
) ENGINE=InnoDB AUTO_INCREMENT=1747 DEFAULT CHARSET=utf8;

#
# Structure for the `tcountry` table : 
#

CREATE TABLE `tcountry` (
  `nIdCountry` int(11) NOT NULL AUTO_INCREMENT,
  `sName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nIdCountry`),
  KEY `I_Country_sName` (`sName`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;

#
# Structure for the `tcurrency` table : 
#

CREATE TABLE `tcurrency` (
  `nIdCurrency` int(11) NOT NULL AUTO_INCREMENT,
  `nIdConvCurrency` int(11) DEFAULT NULL,
  `sCode` varchar(3) NOT NULL,
  `nConvAmount` decimal(11,10) DEFAULT NULL,
  PRIMARY KEY (`nIdCurrency`),
  KEY `fk_tCurrency_tCurrency1_idx` (`nIdConvCurrency`),
  CONSTRAINT `fk_tCurrency_tCurrency1` FOREIGN KEY (`nIdConvCurrency`) REFERENCES `tcurrency` (`nIdCurrency`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

#
# Structure for the `temprates` table : 
#

CREATE TABLE `temprates` (
  `nidhotel` int(11) DEFAULT NULL,
  `dbookdate` datetime DEFAULT NULL,
  `r1` int(11) DEFAULT NULL,
  `r2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tregion` table : 
#

CREATE TABLE `tregion` (
  `nIdRegion` int(11) NOT NULL AUTO_INCREMENT,
  `sName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nIdRegion`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

#
# Structure for the `tusstate` table : 
#

CREATE TABLE `tusstate` (
  `nIdUSState` int(11) NOT NULL AUTO_INCREMENT,
  `sName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nIdUSState`),
  KEY `I_tUSState_sName` (`sName`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

#
# Structure for the `thotel` table : 
#

CREATE TABLE `thotel` (
  `nIdHotel` int(11) NOT NULL AUTO_INCREMENT,
  `nIdRegion` int(11) NOT NULL,
  `nIdCountry` int(11) DEFAULT NULL,
  `nIdCity` int(11) DEFAULT NULL,
  `nIdUSState` int(11) DEFAULT NULL,
  `sCheck` int(11) DEFAULT NULL,
  `sCode` varchar(10) DEFAULT NULL,
  `sName` varchar(200) DEFAULT NULL,
  `sGPS` varchar(45) DEFAULT NULL,
  `sURL` varchar(200) DEFAULT NULL,
  `sURLPict` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`nIdHotel`),
  KEY `fk_tHotel_tRegion1_idx` (`nIdRegion`),
  KEY `fk_tHotel_tCity1_idx` (`nIdCity`),
  KEY `fk_tHotel_tCountry1_idx` (`nIdCountry`),
  KEY `fk_tHotel_tUSState1_idx` (`nIdUSState`),
  KEY `I_tHotel_sCode` (`sCode`),
  CONSTRAINT `fk_tHotel_tCity1` FOREIGN KEY (`nIdCity`) REFERENCES `tcity` (`nIdCity`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tHotel_tCountry1` FOREIGN KEY (`nIdCountry`) REFERENCES `tcountry` (`nIdCountry`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tHotel_tRegion1` FOREIGN KEY (`nIdRegion`) REFERENCES `tregion` (`nIdRegion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tHotel_tUSState1` FOREIGN KEY (`nIdUSState`) REFERENCES `tusstate` (`nIdUSState`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9274 DEFAULT CHARSET=utf8;

#
# Structure for the `tratetype` table : 
#

CREATE TABLE `tratetype` (
  `nIdRateType` int(11) NOT NULL,
  `sCode` varchar(3) NOT NULL,
  PRIMARY KEY (`nIdRateType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `trates` table : 
#

CREATE TABLE `trates` (
  `nIdRate` int(11) NOT NULL AUTO_INCREMENT,
  `nIdHotel` int(11) NOT NULL,
  `nIdCurrency` int(11) NOT NULL,
  `nIdConvCurrency` int(11) DEFAULT NULL,
  `nIdRateType` int(11) DEFAULT NULL,
  `dCheckDate` date DEFAULT NULL,
  `dBookDate` date DEFAULT NULL,
  `nRate` decimal(11,2) DEFAULT NULL,
  `nConvRate` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`nIdRate`),
  KEY `fk_trates_thotel_idx` (`nIdHotel`),
  KEY `fk_tRates_tCurrency1_idx` (`nIdCurrency`),
  KEY `fk_tRates_tCurrency2_idx` (`nIdConvCurrency`),
  KEY `fk_tRates_tRateType1_idx` (`nIdRateType`),
  CONSTRAINT `fk_tRates_tCurrency1` FOREIGN KEY (`nIdCurrency`) REFERENCES `tcurrency` (`nIdCurrency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tRates_tCurrency2` FOREIGN KEY (`nIdConvCurrency`) REFERENCES `tcurrency` (`nIdCurrency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trates_thotel` FOREIGN KEY (`nIdHotel`) REFERENCES `thotel` (`nIdHotel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tRates_tRateType1` FOREIGN KEY (`nIdRateType`) REFERENCES `tratetype` (`nIdRateType`)
) ENGINE=InnoDB AUTO_INCREMENT=494574 DEFAULT CHARSET=cp1251;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;